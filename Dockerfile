FROM openjdk:8-alpine
MAINTAINER stoleas

# JIRA Environments
ENV JIRA_INSTALL_DIR        /opt/atlassian/jira/
ENV JIRA_HOME               /opt/atlassian/jira-home/
ENV JIRA_ARTIFACT_URL       https://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-6.4.14.tar.gz
ENV JIRA_ARTIFACT_NAME      atlassian-jira-6.4.14

# Expose Application ports
EXPOSE 7991

# Install packages and dependencies
RUN apk upgrade  --update && \
    apk add      --update curl wget ca-certificates bash openssl

# Setup JIRA
RUN mkdir -p ${JIRA_INSTALL_DIR} ${JIRA_HOME}/shared/data  && \
    wget  -O /opt/${JIRA_ARTIFACT_NAME}.tgz ${JIRA_ARTIFACT_URL} && \
    tar   -xzf /opt/${JIRA_ARTIFACT_NAME}.tgz -C ${JIRA_INSTALL_DIR} && \
    mv    ${JIRA_INSTALL_DIR}/${JIRA_ARTIFACT_NAME}-standalone ${JIRA_INSTALL_DIR}/${JIRA_ARTIFACT_NAME} && \
    rm    /opt/${JIRA_ARTIFACT_NAME}.tgz

ADD conf/server.xml ${JIRA_INSTALL_DIR}/${JIRA_ARTIFACT_NAME}/conf/server.xml
ADD bin/user.sh ${JIRA_INSTALL_DIR}/${JIRA_ARTIFACT_NAME}/bin/user.sh

CMD ["/opt/atlassian/jira/atlassian-jira-6.4.14/bin/start-jira.sh", "-fg"]

# Build this image
# docker build -t jira:6.4.14 .

# Test - Example Run
# docker run --expose 7991 -p 7991:7991 jira:6.4.14

# Source
# https://bitbucket.org/Stoleas/docker-jira/src